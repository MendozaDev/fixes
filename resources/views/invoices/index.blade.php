@extends('layouts.app')
@section('content')

    <div class="content container">

        <div class="page-header">
            <h1 style="color: white">Facturas<small></small></h1>
        </div><br>
        <div class="row">
            <div class="col-sm-3">
                <form action="#" method="get">
                    <div class="input-group">

                        <input class="form-control" id="system-search" name="q" placeholder="Buscar factura" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                    </div>
                </form>
            </div>

        </div><br>
        <div class="panel panel-primary">
            <div class="panel-heading" style="background-color: #3B5998; color: white">
                <h3 class="panel-title">Indice</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-list-search table-responsive">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Planta</td>
                            <td>Semana</td>
                            <td>Fecha</td>
                            <td>Total servicios</td>
                            <td>Total</td>
                            <td>Estatus</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ $invoice->planta_nombre }}</td>
                                <td>{{ $invoice->number }}</td>
                                <td>
                                    @if($invoice->checkInvoice($invoice->plants_id)->first())
                                        {{ $invoice->checkInvoice($invoice->plants_id)->first()->date }}
                                    @endif
                                </td>
                                <td>{{ $invoice->tokens_count }}</td>
                                <td>{{ $invoice->total }}</td>

                                <td>
                                    @if($invoice->checkInvoice($invoice->plants_id)->first())
                                        <span class="label label-success">Facturado</span>
                                    @else
                                        <span class="label label-warning">Pendiente</span>
                                    @endif
                                </td>

                                <td><div class="dropdown">
                                        <button id="dLabel" type="button"  class="btn btn-default btn-xs" style="padding: 4px 10px; margin: 1px 1px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Acciones
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            @if($invoice->checkInvoice($invoice->plants_id)->first())
                                                <li><a href="{{url('/invoices/edit/'.$invoice->number.'/'.$invoice->plants_id)}}">Editar</a></li>
                                                <li><a href="{{url('/invoices/'.$invoice->checkInvoice($invoice->plants_id)->first()->id)}}">Ver</a></li>
                                                <li>@include('invoices.delete', ['invoice' => $invoice])</li>
                                            @else
                                                <li><a href="{{url('/invoices/edit/'.$invoice->number.'/'.$invoice->plants_id)}}">Crear</a></li>
                                            @endif
                                        </ul>
                                    </div></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


@endsection