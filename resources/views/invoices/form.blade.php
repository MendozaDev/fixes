{!! Form::open (['url' =>'/invoices/edit/'.$weeks_id.'/'.$plant->id, 'method' => 'post']) !!}

<div class="well well-lg">
    <div class="row">
<div class="col-sm-3">
    <strong>Planta</strong><br />
    {{ $plant->name }}
</div>
<div class="col-sm-3">
    <strong>Razón Social</strong><br />
    {{ $plant->social_reason }}
</div>
<div class="col-sm-3">
    <strong>RFC</strong><br />
    {{ $plant->rfc }}
</div>

<div class="col-sm-3">
    <strong>Dirección</strong><br />
    {{ $plant->street }}{{ $plant->ext_number }}
</div>
    </div>
</div>



<div class="col-md-12"><br /><br /></div>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Planta</th>
            <th>Fecha</th>
            <th>Semana</th>
            <th>Zona B</th>
            <th>Importe</th>
        </tr>
    </thead>
    <tbody id="tokens_facturables">
    @foreach($tokens as $token)
        <tr>
            <td>{{ $token->plant->name }}</td>
            <td>{{ $token->date }}</td>
            <td>{{ $weeks_id }}</td>
            <td>{{ $token->getZoneB->name }}</td>
            <td>${{ number_format($token->cost_bill, 2) }}</td>
        </tr>
    @endforeach
        <tfoot>
            <tr>
                <td colspan="3"></td>
                <td style="text-align: right;">Total</td>
                <td style="text-align: right;">${{ number_format($tokens->sum('cost_bill'), 2) }}</td>
            </tr>
        </tfoot>
    </tbody>
</table>

<hr>
<div class="row">
    <div class="col-md-1 col-md-offset-9" align="">
        <a class="btn btn-warning" href="{{ url('/invoices') }}" role="button">Regresar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>

{!! Form::close ()!!}



