@extends('layouts.app')
@section('content')

    <div class="content container">

        <div class="page-header">
            <h1 style="color: white">Zona B <small></small></h1>
        </div><br>
        <div class="row">
            <div class="col-sm-3">
                <form action="#" method="get">
                    <div class="input-group">

                        <input class="form-control" id="system-search" name="q" placeholder="Buscar zona" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                    </div>
                </form>
            </div>
            <div class="col-sm-1 col-sm-offset-7"align="center">
                <a href="{{ url('/zones/create') }}" class="btn btn-info btn-fab">Nueva zona</a>
            </div>
        </div><br>
        <div class="panel panel-primary">
            <div class="panel-heading" style="background-color: #3B5998; color: white">
                <h3 class="panel-title">Indice</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-list-search table-responsive">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Ubicacion</td>
                            <td>Valor</td>
                            <td>Planta</td>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($zones as $zone)
                            <tr>
                                <td>{{ $zone->id }}</td>
                                <td>{{ $zone->name }}</td>
                                <td>{{ $zone->value }}</td>
                                <td>{{ $zone->plants_id }}</td>



                                <td><div class="dropdown">
                                        <button id="dLabel" type="button"  class="btn btn-default btn-xs" style="padding: 4px 10px; margin: 1px 1px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Acciones
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">

                                            <li><a href="{{url('/zones/'.$zone->id.'/edit')}}">Editar</a></li>
                                            <li>@include('zones.delete', ['zone' => $zone])</li>

                                        </ul>
                                    </div></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


@endsection