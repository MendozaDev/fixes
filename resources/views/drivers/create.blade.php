@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Operadores                <small>Nuevo</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('drivers.form',['driver' => $driver, 'url' => '/drivers', 'method' => 'POST'])
                </div>
            </div>
        </div>


@endsection
