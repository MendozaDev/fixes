@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Plantas                 <small>Editar</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('plants.form',['plant' => $plant, 'url' => '/plants/'.$plant->id, 'method' => 'PUT'])
                </div>
            </div>
        </div>


@endsection
