{!! Form::open (['url' => $url, 'method' => $method]) !!}
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('name', $zonea->name,['class'=> 'form-control','placeholder'=>'Nombre', 'title'=>"Nombre", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('value', $zonea->value,['class'=> 'form-control','placeholder'=>'Valor', 'title'=>"Valor", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('enterprises_id', $enterprises_id,null,['class'=> 'form-control','placeholder'=>'Planta', 'title'=>"Planta", 'required' => 'required']) }}
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-1 col-md-offset-9" align="">
        <a class="btn btn-warning" href="{{ url('/zonesa') }}" role="button">Cancelar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>

{!! Form::close ()!!}
