@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Semanas     <small>Nueva</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('weeks.form',['week' => $week, 'url' => '/weeks', 'method' => 'POST'])
                </div>
            </div>
        </div>


@endsection
