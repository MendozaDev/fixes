@extends('layouts.app')
@section('content')
    {!! Form::open (['route' => ['tokens-update-post', $token->id, $step], 'method' => 'post']) !!}
    <section class="container">
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Asiganacion">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-road"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Zona A">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Zona B">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-barcode"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completado">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>


            <div class="tab-content">
                <div class="tab-pane" role="tabpanel" id="step1">
                    <h3>Asignacion</h3>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->date }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->hour }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->service }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->schedule }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <ul class="list-inline pull-right">
                        <li><span class="btn btn-success next-step">Continuar</span></li>
                    </ul>
                </div>




                <div class="tab-pane" role="tabpanel" id="step2">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                <label for="">Driver</label>
                                {{ $token->driver->name }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="form-group">
                            <div class="fg-line">
                                <label for="">Enterprises</label><br />
                                {{ $token->enterprise->name }}
                            </div>
                        </div>
                    </div>





                    <h3>Zona A</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label for="">Folio</label><br />
                                    {{ $token->folio }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label for="">zones a</label><br />
                                    {{ $token->zonea->name }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label for="">wait time a</label><br />
                                    {{ $token->wait_time_a }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label for="">Passengers A</label><br />
                                    {{ $token->passengers_a }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Total a pagar</label>
                                    {{ $token->cost_bill_a }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>

                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Regresar</button></li>
                        <li><span class="btn btn-primary next-step">Continuar</span></li>
                    </ul>
                </div>
                <div class="tab-pane active" role="tabpanel" id="step3">
                    <h3>Zona B</h3>
                    {!! Form::select('weeks_id', $weeks_id, null, ['class' => 'form-control', 'placeholder' => '- Week -']) !!}
                    {!! Form::select('plants_id', $plants_id, null, ['class' => 'form-control', 'placeholder' => '- Plants -']) !!}
                    {!! Form::select('centers_id', $centers_id, null, ['class' => 'form-control', 'placeholder' => '- Centers -']) !!}
                    {!! Form::select('zones_b_id', $zones_b_id, null, ['class' => 'form-control', 'placeholder' => '- Zona B -']) !!}
                    {!! Form::text('other_cost',null,['class' => 'form-control', 'placeholder' => '- Other cost -']) !!}
                    {!! Form::textarea('concept',null, ['class' => 'form-control', 'placeholder' => '- Concept -']) !!}
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Regresar</button></li>

                        <li><button type="button" class="btn btn-primary btn-info-full next-step">Guardar</button></li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                    <h3>Completado</h3>
                    <p>Vale guardado exitosamente</p>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </section>
@endsection