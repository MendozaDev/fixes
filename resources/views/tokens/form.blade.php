{!! Form::open (['url' => $url, 'method' => $method]) !!}
<div class="col-sm-4 ">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('folio', $token->folio,['class'=> 'form-control','placeholder'=>'Folio', 'title'=>"Folio vale fisico", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('plants_id', $plants_id,null,['class'=> 'form-control sumatoria plantaSelector','placeholder'=>'Planta', 'id' => 'planta','title'=>"Planta a la que se le presto servicio", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('weeks_id', $weeks_id,null,['class'=> 'form-control','placeholder'=>'Semana', 'title'=>"Semana de facturacion", 'required' => 'required']) }}
        </div>
    </div>
</div>
<br><br><br>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('date', $token->date,['class'=> 'form-control','placeholder'=>'Fecha de realizacion', 'title'=>"Fecha de servicio", 'id' => 'datepicker', 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('hour', $token->hour,['class'=> 'form-control','placeholder'=>'Hora', 'title'=>"Hora de servicio", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('service',  $service, null, ['class'=> 'form-control','placeholder'=>'Tipo', 'title'=>"Tipo de servicio", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('passengers',  $token->passengers,['class' => 'form-control sumatoria', 'placeholder' =>'Pasajeros', 'id' => 'pasajeros', 'title'=>"Numero de pasajeros", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('drivers_id',  $drivers_id,null,['class' => 'form-control', 'placeholder' =>'Auto', 'title'=>"Auto de presto el servicio", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('suburb', $token->suburb,['class' => 'form-control', 'placeholder' =>'Colonia', 'title'=>"Colonia", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('zones_a', $token->zones_a,['class' => 'form-control sumatoria ', 'placeholder' =>'Zona A', 'id' => 'zona_a', 'required' => 'required', 'title'=>"Zona A"]) }}
        </div>
    </div>
</div>


<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('schedule', $schedule,null,['class' => 'form-control sumatoria ', 'placeholder' =>'Programado', 'id' => 'programado', 'required' => 'required', 'title'=>"El vale estaba programado?", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::select ('centers_id', $centers_id,null,['class' => 'form-control sumatoria ', 'placeholder' =>'Centro de costo', 'id' => 'centro_costo', 'required' => 'required', 'title'=>"Centro de costo", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('wait_time', $token->wait_time,['class' => 'form-control sumatoria', 'placeholder' =>'Tiempo de espera', 'id' => 'tiempo', 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('other_cost', $token->other_cost,['class' => 'form-control sumatoria', 'placeholder' =>'Otro costo', 'id' => 'otro_costo']) }}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('concept', $token->concept,['class' => 'form-control', 'placeholder' =>'Concepto']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 column">
        <table class="table table-hover" id="tab_logic">
            <thead>
            <tr >
                <th class="text-center">
                    #
                </th>
                <th class="text-center">
                    Nombre
                </th>
                <th class="text-center">
                    Dirección
                </th>
                <th class="text-center">
                    Telefono
                </th>
            </tr>
            </thead>
            <tbody>
            <tr id='addr0'>
                <td>
                    1
                </td>
                <td>
                    <input type="text" name='name0'  placeholder='Nombre' class="form-control"/>
                </td>
                <td>
                    <input type="text" name='mail0' placeholder='Dirección' class="form-control"/>
                </td>
                <td>
                    <input type="text" name='mobile0' placeholder='Telefono' class="form-control"/>
                </td>
            </tr>
            <tr id='addr1'></tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-sm-1" align="">
        <a id="add_row" class="btn btn-default pull-left">Agregar</a>
    </div>
    <div class="col-sm-1" align="">
        <a id='delete_row' class="pull-right btn btn-default">Borrar</a>
    </div>
</div>




<hr>

<legend>Cliente</legend>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <div class="fg-line">
                <select name ="zones_b_id" class="form control sumatoria zonaBSelector" data-live-search="true" placeholder="Zona B" id="zona_b" required>
                    <script>
                        var zonesB = {!! $zones_b_json !!}
                    </script>
                    <option value="" disabled selected>Zona B</option>
                    @foreach($zones_b_id as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>

            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <div class="fg-line">
                {{ Form::text ('cost_bill',  $token->cost_bill,['class' => 'form-control', 'placeholder' =>'Costo de factura', 'id' => 'factura', 'required' => 'required']) }}
            </div>
        </div>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-sm-1 col-sm-offset-8" align="">
        <a class="btn btn-warning" href="{{ url('/tokens') }}" role="button">Cancelar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>




{!! Form::close ()!!}


