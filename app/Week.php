<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    public function invoices(){
        return $this->hasMany('App\Token', 'weeks_id', 'id');
    }

    public function checkInvoice($plants_id){
        return $this->hasOne('App\Invoice', 'weeks_id', 'id')->where('plants_id', $plants_id);
    }
}
