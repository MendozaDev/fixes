<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Week;

class WeeksController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function index()
    {
        $weeks = Week::all();
        return view('weeks.index', ['weeks' => $weeks]);
    }

    
    public function create()
    {
        $week = new Week;
        return view('weeks.create', ['week' => $week]);
    }

   
    public function store(Request $request)
    {
        $week = new Week;
        
        $week->number = $request->number;
        $week->date_start = $request->date_start;
        $week->date_end = $request->date_end;
        
        if ($week->save()){
            return redirect('/weeks/create');}
        else{
            return view('/503');
        }
        
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $week = Week::find($id);
        return view('weeks.edit', ['week' => $week]);
    }

    
    public function update(Request $request, $id)
    {
        $week = Week::find($id);

        $week->name = $request->name;
        $week->date_start = $request->date_start;
        $week->date_end = $request->date_end;
        
        if ($week->save()){
            return redirect('/weeks');}
        else{
            return view('weeks.edit');
        }
        
    }

   
    public function destroy($id)
    {
        Week::destroy($id);
        return redirect('/weeks');
    }
}
