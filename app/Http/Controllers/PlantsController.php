<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Plant;

class PlantsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $plants = Plant::all();
        return view('plants.index',["plants" => $plants]);
    }


    public function create()
    {
        $plant = new Plant;
        return view("plants.create", ["plant" => $plant]);
    }


    public function store(Request $request)
    {
        $hasFile = $request->hasFile('picture') && $request->picture->isValid();

        $plant = new Plant;

        $plant-> name = $request-> name;
        $plant-> social_reason = $request-> social_reason;
        $plant-> rfc = $request-> rfc;
        $plant-> street = $request-> street;
        $plant-> ext_number = $request-> ext_number;
        $plant-> int_number = $request-> int_number;
        $plant-> suburb = $request-> suburb;
        $plant-> city = $request-> city;
        $plant-> state = $request-> state;
        $plant-> pc = $request-> pc;
        $plant-> contact_1 = $request-> contact_1;
        $plant-> contact_2 = $request-> contact_2;
        $plant-> mail_1 = $request-> mail_1;
        $plant-> phone_1 = $request-> phone_1;
        $plant-> mail_2 = $request-> mail_2;
        $plant-> phone_2 = $request-> phone_2;

        $plant->user_id = Auth::user()->id;

        $plant->all();

        if ($hasFile){
            $extension = $request->picture->extension();
            $plant->extension = $extension;
        }

        if ($plant->save()){

                if($hasFile){
                    $request->picture->storeAs('images', "$plant->id.$extension");
                }
            return redirect("/plants");}
        else{
            return view ("/503");
        }
    }


    public function show($id)
    {
        $plant = Plant::find($id);

        return view('plants.show', ['plant' => $plant]);
    }


    public function edit($id)
    {
        $plant = Plant::find($id);

        return view("plants.edit", ["plant" => $plant]);
    }


    public function update(Request $request, $id)
    {
        $plant = Plant::find($id);

        $plant-> name = $request-> name;
        $plant-> social_reason = $request-> social_reason;
        $plant-> rfc = $request-> rfc;
        $plant-> street = $request-> street;
        $plant-> ext_number = $request-> ext_number;
        $plant-> int_number = $request-> int_number;
        $plant-> suburb = $request-> suburb;
        $plant-> city = $request-> city;
        $plant-> state = $request-> state;
        $plant-> pc = $request-> pc;
        $plant-> contact_1 = $request-> contact_1;
        $plant-> contact_2 = $request-> contact_2;
        $plant-> mail_1 = $request-> mail_1;
        $plant-> phone_1 = $request-> phone_1;
        $plant-> mail_2 = $request-> mail_2;
        $plant-> phone_2 = $request-> phone_2;

        $plant->all();

        if ($plant->save()){
            return redirect("/plants");}
        else{
            return view ("plants.edit");
        }
    }


    public function destroy($id)
    {
        Plant::destroy($id);
        return redirect('/plants');
    }

    public function getZonasB_ByPlanta(Request $request){
        $zonasB = Plant::where('id', $request->planta)->with('getZonesB')->first();
        return $zonasB->getZonesB->toJson();
    }
}
