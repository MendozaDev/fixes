<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Zone_b;
use App\Plant;
class ZonesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $zones = Zone_b::all();
        return view('zones.index', ["zones" => $zones]);
    }

    
    public function create()
    {
        
        $zone = new Zone_b;
        return view('zones.create', ["zone" => $zone,  "plants_id" => Plant::pluck('name','id')]);

        $zone->plant();
    }


    public function store(Request $request)
    {

        $zone = new Zone_b;

        $zone -> name = $request -> name;
        $zone -> value = $request -> value;
        $zone -> plants_id = $request -> plants_id;

        $zone->all();
        $zone->plant();

        if ($zone->save()){
            return redirect("/zones/create");}
        else{
            return view ("/503");
        }
    }


    public function show($id)
    {
        
    }


    public function edit($id)
    {
        $zone = Zone_b::find($id);
        return view('zones.edit', ["zone" => $zone,  "plants_id" => Plant::pluck ('name')]);
    }
    


    public function update(Request $request, $id)
    {
        $zone = Zone_b::find($id);

        $zone -> name = $request -> name;
        $zone -> value = $request -> value;
        $zone -> plant_id = $request -> plant_id;

        $zone->all();

        if ($zone->save()){
            return redirect("/zones");}
        else{
            return view ("zones.edit");
        }
    }


    public function destroy($id)
    {
        Zone_b::destroy($id);
        return redirect('/zones');
    }
}
