<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Center;
use App\Plant;

class CentersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $centers = Center::all();
        return view('centers.index', ["centers" => $centers]);
    }


    public function create()
    {
        $center = new Center;
        return view('centers.create', ["center" => $center,  "plants_id" => Plant::pluck('name','id')]);
        
        $center->plant();
    }


    public function store(Request $request)
    {
        $center = new Center;

        $center -> name = $request -> name;
        $center -> plants_id = $request -> plants_id;

        $center->all();

        if ($center->save()){
            return redirect("/centers");}
        else{
            return view ("/503");
        }
    }


    public function show($id)
    {
        $center = Center::find($id);
        return view('centers.show', ['center' => $center]);
    }


    public function edit($id)
    {
        $center = Center::find($id);
        return view('centers.create', ["center" => $center,  "plants_id" => Plant::pluck('name','id')]);
    }


    public function update(Request $request, $id)
    {
        $center = Center::find($id);

        $center -> name = $request -> name;
        $center -> plants_id = $request -> plants_id;+

        $center->all();

        if ($center->save()){
            return redirect("/centers");}
        else{
            return view ("centers.edit");
        }
    }


    public function destroy($id)
    {
        Center::destroy($id);
        return redirect('/centers');
    }
}
