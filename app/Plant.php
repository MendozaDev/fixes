<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = 'plants';


    public function getZonesB()
    {
        return $this->hasMany('App\Zone_b', 'plants_id', 'id');
    }
}
