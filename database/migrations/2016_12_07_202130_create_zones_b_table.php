<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonesBTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones_b', function (Blueprint $table){
            $table->increments("id");
            $table->string('name');
            $table->float('value');  
            
            $table->integer('plants_id')->unsigned();

            $table->foreign('plants_id')->references('id')->on('plants');

            $table->timestamps();

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('zones_b');
    }
}
