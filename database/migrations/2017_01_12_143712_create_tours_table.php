<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('tours', function (Blueprint $table){
            $table->increments('id');
            $table->integer('folio')->unique();
            $table->date('date_tour');
            $table->string('place');
            $table->float('price');
            $table->string('extension')->nullable();

            $table->integer('travelers_id')->unsigned()->nullable();

            $table->foreign('travelers_id')->references('id')->on('travelers');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
